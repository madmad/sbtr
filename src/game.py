import ecs
import pygame
from engine.pyg import pygameEngine
from Components import *
from math import sqrt, pow
import random
from copy import deepcopy
import engine.tmx as tmx

SPRITE_SIZE = 32
EVT_SIMEND = 1 # we won the game!
EVT_STATE = 2 # manually switch the program state

class RainSystem(ecs.System):
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Watercloud)
        if pairs == None: return
        for (e, c) in pairs:
            r = entities.component_for_entity(e, Rain)
            if r:
                c.water -= 0.1*dt # 100 raindrops per second
                r.water += 0.1*dt 
                if c.water <= 0:
                    entities.remove_component(e, Watercloud)
                    r = entities.component_for_entity(e, HasRainbow)
                    if r:
                        entities.add_component(r.rainbow, TTL(1000))
                        entities.add_component(r.rainbow, Visual('fading_rainbow'))
                    entities.add_component(e, Visual('cloudpoof'))
                    entities.remove_component(e, Active)
                else:
                    rb = entities.component_for_entity(e, HasRainbow)
                    if not rb:
                        p = entities.component_for_entity(e, Physics)
                        if p: 
                            rb = entities.new_entity()
                            entities.add_component(rb, Rainbow())
                            entities.add_component(rb, Visual('rainbow'))
                            entities.add_component(rb, Physics(p.x, p.y+10))
                            entities.add_component(e, HasRainbow(rb))
                    else:
                        p = entities.component_for_entity(e, Physics)
                        if p:
                            rbp = entities.component_for_entity(rb.rainbow, Physics)
                            rbp.x = p.x
                            rbp.y = p.y+10

class CloudSystem(ecs.System):
    def update(self, dt, entities):
        pairs = entities.pairs_for_type(Watercloud)
        if pairs == None: return
        for (e, c) in pairs:
            r = entities.component_for_entity(e, Rain)
            if r:
                if c.water > 500:
                    entities.add_component(e, Visual('bigraincloud'))
                elif c.water > 250:
                    entities.add_component(e, Visual('mediumraincloud'))
                elif c.water > 0:
                    entities.add_component(e, Visual('smallraincloud'))
                else:
                    entities.remove_component(e, Visual)

            else:
                if c.water > 500:
                    entities.add_component(e, Visual('bigcloud'))
                elif c.water > 250:
                    entities.add_component(e, Visual('mediumcloud'))
                elif c.water > 0:
                    entities.add_component(e, Visual('smallcloud'))
                else:
                    entities.remove_component(e, Visual)

class SetupInteraction(ecs.System):
    def __init__(self, entity_system):
        self.es = entity_system
        self.energy = 300
        self.charging = False

    def getCallbacks(self):
         # EventHandler([pygame.KEYDOWN, pygame.KEYUP], self.handleKeyPress),
        return [
         EventHandler(pygame.MOUSEBUTTONDOWN, self.handleMouseDown),
         EventHandler(pygame.MOUSEMOTION, self.handleMouseMotion),
         EventHandler(pygame.MOUSEBUTTONUP, self.handleMouseUp)
        ]

    def update(self, dt, en):
        pass

    def handleKeyPress(self, entities, event):
        pass

    def handleMouseMotion(self, entities, event):
        # move command
        x, y = event.pos
        if self.charging:
            pairs = self.es.pairs_for_type(Active)
            if not pairs == None: 
                for (e,c) in pairs:
                    speedPos = entities.component_for_entity(e, Physics)
                    speedPos.x = x-(SPRITE_SIZE/2)
                    speedPos.y = y


    def handleMouseDown(self, entities, event):
        if event.button == 1: # left but
            pairs = self.es.pairs_for_type(Active)
            if not pairs == None: 
                for (e,c) in pairs:
                    entities.remove_component(e, Active)

            self.charging = True
            self.mousePos = event.pos
            pairs = self.es.pairs_for_type(Watercloud)
            foundit = False
            for (e,c) in pairs:
                speedPos = entities.component_for_entity(e, Physics)
                x, y = speedPos.x, speedPos.y-16
                rect = pygame.Rect(x,y,32,32)
                if rect.collidepoint(self.mousePos):
                    entities.add_component(e, Active())
                    foundit = True
            if not foundit:
                x, y = event.pos
                e2 = entities.new_entity()
                entities.add_component(e2, Physics(x-(SPRITE_SIZE/2),y))
                entities.add_component(e2, Watercloud(max(0,self.energy)))
                entities.add_component(e2, Rain())
                entities.add_component(e2, Active())
        elif event.button == 3: # right but
            pairs = self.es.pairs_for_type(Watercloud)
            foundit = False
            for (e,c) in pairs:
                speedPos = entities.component_for_entity(e, Physics)
                x, y = speedPos.x, speedPos.y-16
                rect = pygame.Rect(x,y,32,32)
                if rect.collidepoint(event.pos):
                    entities.delete_entity(e)
                    foundit = True
        elif event.button == 4: # mousewheel up
            pairs = self.es.pairs_for_type(Active)
            if not pairs == None: 
                for (e,c) in pairs:
                    cloud = entities.component_for_entity(e, Watercloud)
                    cloud.water += 10
                    cloud.water = max(1, cloud.water)
        elif event.button == 5: # mousewheel down
            pairs = self.es.pairs_for_type(Active)
            if not pairs == None: 
                for (e,c) in pairs:
                    cloud = entities.component_for_entity(e, Watercloud)
                    cloud.water -= 10
                    cloud.water = max(1, cloud.water)
            pass

    def handleMouseUp(self, entities, event):
        self.charging = False

class SimulationInteraction(ecs.System):
    def __init__(self, entity_system):
        self.es = entity_system
        self.rain = False
        self.energy = 0
        self.charging = False

    def getCallbacks(self):
        return [
         EventHandler([pygame.KEYDOWN, pygame.KEYUP], self.handleKeyPress),
         EventHandler(pygame.MOUSEBUTTONDOWN, self.handleMouseDown),
         EventHandler(pygame.MOUSEBUTTONUP, self.handleMouseUp)
        ]

    def update(self, dt, en):
        pass

    def handleKeyPress(self, entities, event):
        pass

    def handleMouseDown(self, entities, event):
        if event.button == 1: # left but
            pairs = self.es.pairs_for_type(Active)
            if not pairs == None: 
                for (e,c) in pairs:
                    entities.remove_component(e, Active)

            pairs = self.es.pairs_for_type(Watercloud)
            for (e,c) in pairs:
                speedPos = entities.component_for_entity(e, Physics)
                x, y = speedPos.x, speedPos.y-16
                rect = pygame.Rect(x,y,32,32)
                if rect.collidepoint(event.pos):
                    entities.add_component(e, Active())

    def handleMouseUp(self, entities, event):
        pass

class GoalSystem(ecs.System):
    def update(self, dt, ent):
        themaps = ent.pairs_for_type(Map)
        if themaps == None: return
        (e, mappus) = themaps[0]
        themap = mappus.map
        things = ent.pairs_for_type(Leprachaun)
        moving = False
        someoneleft = False
        if things == None:
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_SIMEND}))
            return
        for (e, l) in things:
            c = ent.component_for_entity(e, Physics)
            if not c: continue
            someoneleft = True
            moving = moving or (abs(c.dx) + abs(c.dy) > 0)
            if ent.component_for_entity(e, Walking):
                rect = pygame.Rect(c.x, c.y, SPRITE_SIZE, SPRITE_SIZE)
                for cell in themap.collide(rect, 'goal'):
                    if l.evil:
                        fa = ent.new_entity()
                        ent.add_component(fa, Physics(cell.px+16, cell.py))
                        ent.add_component(fa, Sound('gold'))
                        ent.add_component(fa, Effect('rainbowSparkle'))
                        ent.remove_component(e, Walking)
                        ent.remove_component(e, Physics)
                        ent.add_component(e, Success())
                    else:
                        fa = ent.new_entity()
                        ent.add_component(fa, Physics(cell.px+16, cell.py))
                        ent.add_component(fa, Effect('rainbowSparkle'))
                        ent.add_component(fa, Sound('fanfare'))
                        ent.add_component(e, Success())
                        ent.remove_component(e, Physics)
                        ent.remove_component(e, Walking)
        if someoneleft == False:
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_SIMEND}))
        if ent.pairs_for_type(Rainbow) == None and not moving:
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_SIMEND}))

                    
class MovementSystem(ecs.System):
    def __init__(self):
        pass

    def update(self, dt, ent):
        themaps = ent.pairs_for_type(Map)
        if themaps == None: return
        (e, mappus) = themaps[0]
        themap = mappus.map
        things = ent.pairs_for_type(Physics)
        if things == None: return
        for (e, c) in things:
            newx = c.x + c.dx
            newy = c.y + c.dy
            if ent.component_for_entity(e, Walking):
                rect = pygame.Rect(newx, newy, SPRITE_SIZE, (SPRITE_SIZE/2))
                for cell in themap.collide(rect, 'blocker'):
                    if (c.x+32) <= cell.left and newx+32 > cell.left:
                        newx = cell.left-32
                    if c.x >= cell.right and newx < cell.right:
                        newx = cell.right
                    if c.y+16 <= cell.top and newy+16 > cell.top:
                        newy = cell.top-16
                    if c.y >= cell.bottom and newy < cell.bottom:
                        newy = cell.bottom

            c.x = newx
            c.y = newy
                    
class TTLSystem(ecs.System):
    def update(self, dt, en):
        things = en.pairs_for_type(TTL)
        if things == None: return
        for (e, c) in things:
            c.ttl -= dt
            if c.ttl < 0: en.delete_entity(e)
        
class EventSystem(ecs.System):
    def __init__(self, entities, game):
        self.entities = entities
        e = entities.new_entity()
        entities.add_component(e, EventHandler(pygame.QUIT, self.handleQuitEvent))
        self.game = game

    def update(self, dt, ent):
        for event in pygame.event.get():
            pairs = self.entities.pairs_for_type(EventHandler)
            if pairs == None: return
            for (e, c) in pairs:
                if type(c.event) == list:
                    if event.type in c.event:
                        c.cbHndl(self.entities, event)
                elif c.event == event.type:
                    c.cbHndl(self.entities, event)

    def handleQuitEvent(self, entities, event):
        pass

class LeprachaunAI(ecs.System):
    def update(self, dt, en):
        leprachauns = en.pairs_for_type(Leprachaun)
        if leprachauns == None: return
        for (e, c) in leprachauns:
            p = en.component_for_entity(e, Physics)
            if not p: continue # Leprachaun isn't live anymore
            p.dx = p.dy = 0
            rpos=[]
            themaps = en.pairs_for_type(Map)
            if themaps == None: return
            (_, mappus) = themaps[0]
            themap = mappus.map
            rainbows = en.pairs_for_type(Rainbow)
            if rainbows: 
                for (rent, rrr) in rainbows:
                    rp = en.component_for_entity(rent, Physics)
                    d = sqrt(pow(rp.x - p.x, 2) + pow(rp.y - p.y, 2))
                    rpos.append((d,rp))
            s = sorted(rpos)
            for goal in themap.find('goal'):
                d = sqrt(pow(goal.px - p.x, 2) + pow(goal.py - p.y, 2))
                if d < 100: 
                    s.insert(0,(d,Physics(goal.px, goal.py)))
            if s:
                (d, rp) = s[0]
                if d > 17:
                    p.dx = (rp.x - p.x)/d * c.speed * dt 
                    p.dy = (rp.y - p.y)/d * c.speed * dt 
            self.setWalking(en, e, p)
            drunk = en.component_for_entity(e, Drunk)
            if drunk:
                drunk.dx = random.gauss(0, drunk.amount/80)
                drunk.dy = random.gauss(0, drunk.amount/80)
                p.dx += drunk.dx * p.dy
                p.dy += drunk.dy * p.dx

    def setWalking(self, en, e, p):
        """ Hilfs-Funktion, zum unuebersichtlich machen """
        state = "leprachaun"
        if abs(p.dx) > abs(p.dy):
            if p.dx > 0:
                state="leprachaun-right"
            elif p.dx < 0:
                state="leprachaun-left"            
        else:
            if p.dy > 0:
                state="leprachaun-front"
            elif p.dy < 0:
                state="leprachaun-back"

        en.add_component(e, Visual(state))
                
class TitleScreen(ecs.System):
    def __init__(self):
        self.ran = False
        self.dt = 0
        
    def update(self, dt, entities):
        if not self.ran:
            a = entities.new_entity()
            entities.add_component(a, Physics(1024/2,768/2))
            entities.add_component(a, Effect(size=1000))
            self.ran = True
        self.dt += dt
        if self.dt > 1500:
            pygame.event.post(pygame.event.Event(pygame.USEREVENT, {'msg': EVT_STATE}))

class WinScreen(ecs.System):
    def __init__(self):
        self.last = 0
        
    def update(self, dt, entities):
        self.last += dt
        if self.last>(1000*random.random()):
            a = entities.new_entity()
            entities.add_component(a, Physics(random.randint(0,1024), random.randint(0,768)))
            entities.add_component(a, Effect(size=random.randint(0,400)))
            entities.add_component(a, TTL(1000))
            self.last = 0

class DebugSystem(ecs.System):
    def update(self, dt, entities):
        print("Update after %f" % dt)

class Game(object):
    def __init__(self):
        self.states = { 
            "title": ["cloud","render", "sound", "titlescreen"],
            "setup": ["cloud", "render", "sound", "interaction"],
            "pause": ["cloud", "render", "sound"],
            "run":   ["cloud", "rain", "movement", "render", "sound", "leprachaun", "ttl", "goal", "siminteraction"],
            "won":   ["render", "sound", "winscreen", "ttl"],
            "ended": ["render", "sound", "ttl"]
        }
        
        self.statetrans = {
            "setup": "run",
            "run": "pause",
            "pause": "run",
            "ended": "setup",
            "title": "setup",
            "won": "setup"
        }

        self.state = "title"

        self.eng = pygameEngine()
        self.entity  = ecs.EntityManager()
        
        self.systems = {"rain": RainSystem(),
                   "cloud": CloudSystem(),
                   "movement": MovementSystem(),
                   "titlescreen": TitleScreen(),
                   "winscreen": WinScreen(),
                   "render": self.eng.renderer(),
                   "sound": self.eng.sounder(),
                   "leprachaun": LeprachaunAI(),
                   "ttl": TTLSystem(),
                   "interaction": SetupInteraction(self.entity),
                   "siminteraction": SimulationInteraction(self.entity),
                   "goal": GoalSystem()}

        self.eventhandlers = {}
        self.machines={}
        for k in self.states.keys():
            self.machines[k] = ecs.SystemManager(self)
            self.eventhandlers[k]=[]
            for v in self.states[k]:
                self.machines[k].add_system(self.systems[v])
                try:
                    foo = self.systems[v].getCallbacks()
                except AttributeError:
                    continue
                self.eventhandlers[k].extend(foo)

        # Setup Event Handling
        for k,v in self.eventhandlers.iteritems():
            v.extend([ EventHandler([pygame.KEYDOWN, pygame.KEYUP], self.handleKeyPress),
                       EventHandler(pygame.QUIT, self.handleQuitEvent),
                       EventHandler(pygame.USEREVENT, self.handleUserEvent)
                     ])

        self.populate()
        self.clock = self.eng.clocker()
        self.running=True
        
    def events(self):
        for event in pygame.event.get():
            for c in self.eventhandlers[self.state]:
                if type(c.event) == list:
                    if event.type in c.event:
                        c.cbHndl(self.entity, event)
                elif c.event == event.type:
                    c.cbHndl(self.entity, event)        

    def run(self):
        while self.running:
            self.machines[self.state].update_systems(self.clock.tick(40), self.entity)
            self.events()

    def statetransition(self, newstate=""):
        if not newstate: newstate = self.statetrans[self.state]
        if newstate == "setup" and self.state == "setup":
            self.entity.database = deepcopy(self.origstate)
        if newstate == "run" and self.state == "setup":
            self.oldstate = deepcopy(self.entity.database)
        if newstate == "setup" and (self.state == "run" or self.state == "pause" or self.state == "ended" or self.state == "won" or self.state == "lost"):
            self.entity.database = deepcopy(self.oldstate)

        self.state = newstate

    def populate(self):
        file = "map1"
        mm = self.eng.load_map(file)
        e = self.entity.new_entity()
        self.entity.add_component(e, Map(mm))

        spawn = (0,0)
        water = 500
        self.tilemap = tmx.load('assets/'+file+'.tmx', (300, 200))

        if  'triggers' in self.tilemap.layers:
            for tile in self.tilemap.layers['triggers'].find('spawn1'):
                spawn = (tile.x * 32, tile.y * 32)

        if 'clouds' in self.tilemap.layers:
            for obj in self.tilemap.layers['clouds'].objects:
                pos = obj.px, obj.py
                if 'dx' in obj:
                    dx, dy = obj['dx'], obj['dy']
                if 'water' in obj:
                    water = obj['water']
                e = self.entity.new_entity()
                self.entity.add_component(e, Watercloud(water))
                self.entity.add_component(e, Rain())
                self.entity.add_component(e, Physics(pos[0],pos[1],dx,dy))
        

        l = self.entity.new_entity()
        self.entity.add_component(l, Physics(spawn[0],spawn[1],0,0))
        self.entity.add_component(l, Visual('leprachaun'))
        self.entity.add_component(l, Leprachaun(0.2))
        self.entity.add_component(l, Walking())

        # l = self.entity.new_entity()
        # self.entity.add_component(l, Physics(100,500,0,0))
        # self.entity.add_component(l, Visual('leprachaun'))
        # self.entity.add_component(l, Leprachaun(0.04, evil=True))
        # self.entity.add_component(l, Walking())

        self.origstate = deepcopy(self.entity.database)

    def handleUserEvent(self, entities, event):
        if event.dict['msg'] == EVT_SIMEND:
            if(entities.pairs_for_type(Success)):
                self.statetransition("won")
            else:
                self.statetransition("ended")
        if event.dict['msg'] == EVT_STATE:
            s = event.dict.get('state', '')
            self.statetransition(s)

    def handleKeyPress(self, entities, event):
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            self.running = False

        if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            self.statetransition()

        if event.type == pygame.KEYDOWN and event.key == pygame.K_r:
            self.statetransition("setup")


    def handleQuitEvent(self, entities, event):
        self.running=False


def main():
    g = Game()
    g.run()

main()
