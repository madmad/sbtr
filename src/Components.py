import ecs

class AmbientSound(ecs.Component):
    def __init__(self, sound=""):
        self.sound = sound

class Sound(ecs.Component):
    def __init__(self, sound=""):
        self.sound = sound
        
class Music(ecs.Component):
    def __init__(self, song=""):
        self.song = song

class Physics(ecs.Component):
    def __init__(self, x=0, y=0, dx=0, dy=0):
        self.x  = x
        self.y  = y
        self.dx = dx
        self.dy = dy

class Watercloud(ecs.Component):
    def __init__(self, water = 100):
        self.water = water

class Rain(ecs.Component):
    def __init__(self, water = 0):
        self.water = water

class Rainbow(ecs.Component):
    pass

class Visual(ecs.Component):
    """ an animation """
    def __init__(self, look=""):
        self.look = look

class Effect(ecs.Component):
    """ a visual effect """
    def __init__(self, effect="", size=64, duration=1):
        self.effect = effect
        self.duration = duration
        self.size = size

class Leprachaun(ecs.Component):
    def __init__(self, speed=1, evil=False):
        self.evil = evil # is this an evil leprachaun?
        self.speed = speed # how fast he can run

class EventHandler(ecs.Component):
    def __init__(self, event, cbHndl):
        self.event = event
        self.cbHndl = cbHndl

class TTL(ecs.Component):
    def __init__(self, ttl):
        self.ttl = ttl

class HasRainbow(ecs.Component):
    """ couples a rainbow to its cloud """
    def __init__(self, rainbow):
        """ Params: rainbow -> entity id of the rainbow """
        self.rainbow = rainbow

class Active(ecs.Component):
    """ this entity is the currently active one, for player interaction """
    pass

class Map(ecs.Component):
    """ The loaded map """
    def __init__(self, tiles):
        self.map = tiles

class Walking(ecs.Component):
    """ Are these boots made for walking? """
    pass

class Success(ecs.Component):
    """ Was this Leprachaun successful? """
    pass

class Drunk(ecs.Component):
    def __init__(self, amount = 1):
        self.amount = amount
        self.dx = amount
        self.dy = amount
