import pygame
from Components import *
import ecs
import tmx
import math

class AnimationState(ecs.Component):
    def __init__(self, animation):
        self.animation = animation
        self.state = 0
        self.t = 0

class EffectState(ecs.Component):
    def __init__(self, animation="rainbowSparkle"):
        self.animation = animation
        self.state = 0
        self.t = 0

class pygRenderer(ecs.System):
    def __init__(self):
        self.screen = pygame.display.set_mode((1024, 768))

        self.font = pygame.font.Font(pygame.font.get_default_font(), 16)
        self.smallfont = pygame.font.Font(pygame.font.get_default_font(), 10)

        self.overlay = tmx.Tileset.load('assets/overlay.tsx')
        self.leprachaun_right = tmx.Tileset.load('assets/leprachaun-right.tsx')
        self.leprachaun_front = tmx.Tileset.load('assets/leprachaun-front.tsx')
        self.leprachaun_back = tmx.Tileset.load('assets/leprachaun-back.tsx')
        self.leprachaun_left = tmx.Tileset.load('assets/leprachaun-left.tsx')
        
        self.animations = {
            'bigraincloud':     { 'delay': 175,
                                  'loop': True,
                                  'z': 3,
                                  'tileset': self.overlay,
                                  'sprites': [9,10,11,12] },
            'mediumraincloud':  { 'delay': 175,
                                  'loop': True,
                                  'z': 3,
                                  'tileset': self.overlay,
                                  'sprites': [17,18,19,20] },
            'smallraincloud' :  { 'delay': 175,
                                  'loop': True,
                                  'z': 3,
                                  'tileset': self.overlay,
                                  'sprites': [25,26,27,28] },
            'smallcloud':       { 'delay': 10000,
                                  'loop': True,
                                  'z': 3,
                                  'tileset': self.overlay,
                                  'sprites': [29] },
            'mediumcloud':      { 'delay': 10000,
                                  'loop': True,
                                  'z': 3,
                                  'tileset': self.overlay,
                                  'sprites': [21] },
            'bigcloud':         { 'delay': 10000,
                                  'loop': True,
                                  'z': 3,
                                  'tileset': self.overlay,
                                  'sprites': [13] },
            'cloudpoof':        { 'delay': 175,
                                  'loop': False,
                                  'z': 3,
                                  'tileset': self.overlay,
                                  'sprites': [33,34,35,36] },
            'leprachaun':       { 'delay': 10000,
                                  'loop': True,
                                  'z': 10,
                                  'tileset': self.overlay,
                                  'sprites': [5] },
            'rainbow':          { 'delay': 500,
                                  'loop': True,
                                  'z': 0,
                                  'tileset': self.overlay,
                                  'sprites': [8] },
            'fading_rainbow':   { 'delay': 175,
                                  'z': 0,
                                  'loop': False,
                                  'tileset': self.overlay,
                                  'sprites': [8,16,24,32] },
            'leprachaun':       { 'delay': 10000,
                                  'loop': True,
                                  'z': 10,
                                  'tileset': self.leprachaun_front,
                                  'sprites': [1] },
            'leprachaun-right': { 'delay': 175,
                                  'loop': True,
                                  'z': 0,
                                  'tileset': self.leprachaun_right,
                                  'sprites': [0,1,2,3,4,5] },
            'leprachaun-back':  { 'delay': 175,
                                  'loop': True,
                                  'z': 0,
                                  'tileset': self.leprachaun_back,
                                  'sprites': [0,1,2,3,4,5] },
            'leprachaun-front': { 'delay': 175,
                                  'loop': True,
                                  'z': 0,
                                  'tileset': self.leprachaun_front,
                                  'sprites': [0,1,2,3,4,5] },
            'leprachaun-left':  { 'delay': 175,
                                  'loop': True,
                                  'z': 0,
                                  'tileset': self.leprachaun_left,
                                  'sprites': [0,1,2,3,4,5] }
        }


    def update(self, dt, entities):
        self.screen.fill((0,255,0))
        self.tilemap.update(dt / 1000., self)
        self.tilemap.draw(self.screen)
        
        self.updateAnimations(dt, entities)
        self.updateSelection(dt, entities)
        self.updateUI(dt, entities)
        self.updateGFX(dt, entities)
        pygame.display.flip()

    def load_map(self, path):
        self.tilemap = tmx.load('assets/'+path, (300, 200))
        self.tilemap.layers['map'].set_view(0,0,1024,768)
        return self.tilemap

    def updateGFX(self, dt, entities):
        pairs = entities.pairs_for_type(Effect)
        if pairs:
            for (e, c) in pairs:
                pos = entities.component_for_entity(e, Physics)
                ef =  entities.component_for_entity(e, EffectState)
                if not ef:
                    ef = EffectState()
                    entities.add_component(e, ef)
                if self.rainbowSparkle(dt, pos, ef, c.size, 1000.0 * c.duration):
                    entities.remove_component(e, EffectState)
                    entities.remove_component(e, Effect)
        
    def change_alpha(self, surface, alpha=0.5):
        for a in pygame.surfarray.pixels_alpha(surface):
            a *= alpha
        return surface        
        
    def updateUI(self, dt, entities):
        downthedrain = 0
        pairs = entities.pairs_for_type(Rain)
        if pairs:
            for (e, c) in pairs:
                downthedrain += c.water
        text = self.font.render("%i drops of water " % downthedrain, True, (20,250,250))
        self.screen.blit(text, (20,20))

    def updateSelection(self, dt, entities):
        pairs = entities.pairs_for_type(Active)
        if pairs == None: return
        for (e, c) in pairs:
            p = entities.component_for_entity(e, Physics)
            if not p: continue
            pygame.draw.rect(self.screen, (0,250,0), (p.x, p.y -16, 32,32), 1)
            cloud = entities.component_for_entity(e, Watercloud)
            if cloud:
                text = self.smallfont.render("%i" % cloud.water, True, (0, 0, 255))
                self.screen.blit(text, (p.x, p.y+16))
                
    def updateAnimations(self, dt, entities):
        surfaces = []
        pairs = entities.pairs_for_type(Visual)
        if pairs == None: return
        for (e, c) in pairs:
            p = entities.component_for_entity(e, Physics)
            if not p: continue
            anim = entities.component_for_entity(e, AnimationState)            
            if not anim or anim.animation != c.look:
                anim = AnimationState(c.look)
                entities.add_component(e, anim)
                    
            i = anim.state
            z = self.animations[anim.animation]['z']
            surfaces.append((z,self.animations[anim.animation]['tileset'].get_tile(self.animations[anim.animation]['sprites'][i]).surface, (p.x, p.y)))
            anim.t += dt
            if anim.t > self.animations[anim.animation]['delay']:
                anim.t = 0
                anim.state = (anim.state + 1) 
                if anim.state == len(self.animations[anim.animation]['sprites']):
                    if self.animations[anim.animation]['loop']:
                        anim.state=0
                    else:
                        entities.remove_component(e, Visual)
                        entities.remove_component(e, AnimationState)
        for (z, a, (x,y)) in sorted(surfaces):
            self.screen.blit(a, (x, y-16))

            
    def rainbowSparkle(self, dt, position, state, esize, etime):
        x = position.x
        y = position.y
        size = math.trunc(max(esize * state.t / etime, 1))
        surf = self.animations['rainbow']['tileset'].get_tile(self.animations['rainbow']['sprites'][0]).surface
        big = pygame.transform.smoothscale(surf, (size, size))
        big = self.change_alpha(big, min(1, 1-(state.t/etime)))
        self.screen.blit(big,(x-size/2,y-size/2))
        state.t += dt
        return state.t > etime
