import pygame
import pygRender
import pygSound

class pygameEngine(object):
    def __init__(self):
        pygame.init()
        self.render = None
        self.clock = None
        self.sound = None

    def load_map(self, path):
        """ who needs a map? """
        return (self.render.load_map(path+".tmx")).layers['map']

    def renderer(self):
        if self.render == None:
            self.render = pygRender.pygRenderer()
        return self.render

    def clocker(self):
        if self.clock == None:
            self.clock = pygame.time.Clock()
        return self.clock

    def sounder(self):
        if self.sound == None:
            self.sound = pygSound.pygSound()
        return self.sound


