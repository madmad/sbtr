import pygame
import ecs
from Components import *

class DummySound(object):
    ''' empty sound object, to handle 404 '''
    def __init__(self):
        pass
    def play(self):
        pass

class pygSound(ecs.System):
    def __init__(self):
        pygame.mixer.init()
        self.sounds={}           

    def update(self, dt, e):
        self.updateSounds(dt, e)
        self.updateSongs(dt, e)

    def updateSounds(self, dt, entities):
        pairs = entities.pairs_for_type(Sound)
        if pairs == None: return
        for (e, c) in pairs:
            self.playSound(c.sound)
            entities.remove_component(e,type(c))

    def updateSongs(self, dt, entities):
        pairs = entities.pairs_for_type(Music)
        if pairs == None: return
        for (e, c) in pairs:
            if not pygame.mixer.music.get_busy():
                self.playMusic(c.song)

    def loadSound(self, name, filename):
        try:
            self.sounds[name]=pygame.mixer.Sound("assets/" + filename)
        except:
            self.sounds[name] = DummySound()

    def playSound(self, name):
        try:
            self.sounds[name].play()
        except KeyError:
            self.loadSound(name, name + ".ogg")
            self.sounds[name].play()            

    def playMusic(self, filename):
        try:
            pygame.mixer.music.load("assets/"+filename+".ogg")
            pygame.mixer.music.play()
        except pygame.error:
            pass
