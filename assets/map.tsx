<?xml version="1.0" encoding="UTF-8"?>
<tileset name="sbtr_maptiles" tilewidth="32" tileheight="32" spacing="1" margin="1">
 <properties>
  <property name="margin" value="1"/>
  <property name="spacing" value="1"/>
 </properties>
 <image source="assets/sbtr_tile.png" trans="ff00ff" width="265" height="199"/>
 <tile id="0">
  <properties>
   <property name="goal" value=""/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="empty" value=""/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="spawn1" value=""/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="spawn2" value=""/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="spawn3" value=""/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="spawn4" value=""/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="spawn5" value=""/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="spawn6" value=""/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="37">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="blocker" value=""/>
  </properties>
 </tile>
</tileset>
