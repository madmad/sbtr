<?xml version="1.0" encoding="UTF-8"?>
<tileset name="leprchaun-front" tilewidth="32" tileheight="32" firstgid="0">
 <properties>
  <property name="margin" value="0"/>
  <property name="spacing" value="0"/>
 </properties>
 <image source="assets/Leprechaun-front.png" trans="ff00ff" width="192" height="32"/>
</tileset>
