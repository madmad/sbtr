<?xml version="1.0" encoding="UTF-8"?>
<tileset name="sbtr_overlaytiles" tilewidth="32" tileheight="32" spacing="1" margin="1" firstgid="0">
 <properties>
  <property name="margin" value="1"/>
  <property name="spacing" value="1"/>
 </properties>
 <image source="assets/sbtr_overlay.png" trans="ff00ff" width="265" height="199"/>
</tileset>
